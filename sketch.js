// Affichage selon la date => donner la possibilité de tout afficher aussi.

//map
const options = {
    lat: 46,
    lng: 2,
    zoom: 0,
    style: 'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png'
}
//more tile here : https://leaflet-extras.github.io/leaflet-providers/preview/


// Create an instance of Leaflet
const mappa = new Mappa('Leaflet');
let myMap;
let canvas;

//data
let data;

//design
let alpha = 127;
let colorFell;
let colorFound;

//IxD
let timeline, timelineLabel;
let maxTone, v;
let sel;

function preload() {
    //  data = loadTable('../Dataset/Meteorite_Landings.csv', 'csv', 'header');
    data = loadTable('./Dataset/Meteorite_Landings.csv', 'csv', 'header');
    /**
     * Attention votre chemine d'accès n'est pas valide ../Dataset remonte un dossier alors que vous devez descendre d'un dossier ./Dataset
     */
}

function setup() {
    canvas = createCanvas(windowWidth, windowHeight - 50);

    // //design
    colorFell = color('orange');
    colorFound = color('white');

    //IxD
    let UI = createDiv('');
    timeline = createSelect();
    timelineLabel = createSpan("Year: ");

    // for (let r = 0; r < data.getRowCount(); r++) {
    //     let date = data.getString(r, 5);// Récup de la date dans le CSV
    //     timeline.option(date);
    // }
    /**
     * Attention le code ci-dessus n'est pas correct pour plusieurs raisons :
     * 1 - la date n'est pas castée en nombre ce qui donne une valeur au format text. Ainsi timeline.value() renvoie toujours undefined
     * 2 - Toutes les dates sont ajoutées, même celle qui sont identique, ce qui rajoute des options double invisible au select 
     * 
     * Pour corriger cela nous allons uniquement prendre les date unique du CSV et les caster en nombre.
     * Le plus
     */

    let dates = []; //on créer un tableau vide temporarire pour stocker les dates
    for (let r = 0; r < data.getRowCount(); r++) {
        let date = Number(data.getString(r, 5));// Récup de la date dans le CSV que l'on cast en nombre
        let isIncludeInDates = dates.includes(date);// on regarde si la date existe dans le tableau. REF https://www.w3schools.com/jsref/jsref_includes_array.asp
        if(isIncludeInDates == false){
            dates.push(date);// si la date n'est pas présente dans le tableau on l'ajoute au tableau pour la prochaine comparaison
           
        }
    }

    dates.sort(); //on tri le tableau dans l'ordre
    for (let i = 0; i < dates.length; i++) {
        timeline.option(dates[i]); // on ajoute l'option au select
    };

    console.log(dates)

    maxTone = createSlider(0, 100, 1);
    toneLabel = createSpan("Max Tone: " + maxTone.value());


    timeline.parent(UI);
    timelineLabel.parent(UI);
    maxTone.parent(UI);
    toneLabel.parent(UI);

    timeline.input(updateTimeline);
    maxTone.input(updateMaxTone);


    //map
    myMap = mappa.tileMap(options);
    myMap.overlay(canvas);
    myMap.onChange(drawDataviz);

}

function drawDataviz() {
    clear();

    noStroke();
    rectMode(CENTER);


    for (let row = 0; row < data.getRowCount(); row++) {
        let date = data.getString(row, 5);
        if (date == timeline.value() || date == undefined) {
            const long = Number(data.getString(row, 7));
            const lat = Number(data.getString(row, 6));
            if (myMap.map.getBounds().contains({ lat: lat, lng: long })) {

                const pos = myMap.latLngToPixel(lat, long);

                let place = data.getString(row, 0);
                let mass = data.getString(row, 2);
                let fell = (data.getString(row, 3) === 'Fell') ? true : false;

                let rad = sqrt(mass) / PI;
                rad *= 0.025;
                rad = constrain(rad, 2, width) + myMap.zoom();

                noStroke();
                if (fell) {
                    fill(colorFell);
                } else {
                    fill(colorFound);
                }

                if (date != undefined) {
                    ellipse(pos.x, pos.y, rad * 2, rad * 2);
                } else {
                    rect(pos.x, pos.y, rad * 2, rad * 2);
                }

                if (mass < maxTone.value() * 1000) {
                    fill('white');
                    noStroke();
                    textAlign(LEFT, CENTER);
                    textSize(8 + myMap.zoom());
                    text(place + ", " + mass + "kg", pos.x + rad * 1.25 + 10, pos.y);
                    stroke('white');
                    line(pos.x, pos.y, pos.x + rad * 1.25, pos.y);
                }
            }
        }
    }


    //legend
    noStroke();
    fill(colorFell)
    rect(20, height - 20, 20, 20);
    fill(colorFound)
    rect(20, height - 40, 20, 20);
    fill(0);
    textAlign(LEFT, CENTER)
    text("Fell", 40, height - 20);
    text("Found", 40, height - 40);
}

function updateTimeline() {
    updateAll(timelineLabel, "Year: " + timeline.value(), drawDataviz);
}

function updateMaxTone() {
    updateAll(toneLabel, "Max Tone: " + maxTone.value(), drawDataviz);
}

function updateAll(label, data, callback) {
    label.html(data);
    if (typeof callback == "function")
        callback()
}